CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------
This module provides a field group display formatter that ...

REQUIREMENTS
------------
This module requires the Field Group module (https://drupal.org/project/field_group)

INSTALLATION
------------
 * Install as usual, see http://drupal.org/node/70151 for further information.

CONFIGURATION
-------------
 * To create a ...

MAINTAINERS
-----------
Current maintainers:
 * 

